# recipe-api-proxy-demo

this is demo for recipe api proxy nginx

## usage
### environment variables
* `LISTEN_PORT` - port to listen on (default:`8000`)
* `APP_HOST` - hostname of app to forward requests to  (default: `app`)
* `APP_PORT` - port of app to forward requests (default: `9000`)